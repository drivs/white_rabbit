<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit3.php");

use PHPUnit\Framework\TestCase;
use WhiteRabbit3;

class WhiteRabbit3Test extends TestCase
{
    /** @var WhiteRabbit3 */
    private $whiteRabbit3;

    public function setUp() : void
    {
        parent::setUp();
        $this->whiteRabbit3 = new WhiteRabbit3();

    }

    //SECTION FILE !
    /**
     * @dataProvider multiplyProvider
     */
    public function testMultiply($expected, $amount, $multiplier){
        $this->assertEquals($expected, $this->whiteRabbit3->multiplyBy($amount, $multiplier));
    }

    public function multiplyProvider(){
        // Test failes when $i equals 7.
        $arr = array();

        for ($i = 0; $i < 20; $i++) {
            for ($j = 0; $j < 20; $j++) {
                $arr[] = array($i * $j, $i, $j);
            }
        }

        return $arr;
    }
}
