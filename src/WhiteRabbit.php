<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath) : string
    {
        // Read file contents into string variable $text
        $text = file_get_contents($filePath);
        // Convert all uppercase to lowercase for easier, consistent
        // comparison.
        $text = strtolower($text);
        return $text;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        //TODO implement this!

        // Split the string $parsedFile into an array named $text
        // so it's easier to iterate the text.
        $text = str_split($parsedFile);

        $letters = array();

        for($i = 0; $i < 26; $i++) {
            $letters[] = array("letter" => chr($i + 97), "count" => 0);
        }

        // Loop through text one character at a time. If character is part
        // of the set a-z, add 1 to the proper letter's "count" attribute.
        // 97 is the ascii code for 'a' and 122 for 'z', hence the limits
        // in the if statement.
        foreach ($text as $char) {
            if (ord($char) >= 97 && ord($char) <= 122) {
                $letters[ord($char) - 97]["count"] += 1;
            }
        }

        $cmp = function($x, $y) {
            $a = $x["count"];
            $b = $y["count"];

            if ($a == $b)
                return 0;
            return ($a < $b) ? -1 : 1;
        };

        // Sort array using user-defined function "cmp" to compare
        // two array's "count" attribute, and thereby sort $letters
        // by non-decreasing order with respect to the number of
        // occurrences of the letter.
        usort($letters, $cmp);

        // Find index of the median occurring letter
        $index = count($letters) / 2;

        print_r($letters);

        // Pass number of occurrences as a reference
        $occurrences = $letters[$index]["count"];
        // Return letter whose occurrences are the median
        return $letters[$index]["letter"];



    }
}